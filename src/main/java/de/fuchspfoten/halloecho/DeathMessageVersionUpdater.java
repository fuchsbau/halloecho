package de.fuchspfoten.halloecho;

import de.fuchspfoten.fuchslib.data.DataFile;

import java.util.function.BiConsumer;

/**
 * Updates the version of the death message file.
 */
public class DeathMessageVersionUpdater implements BiConsumer<Integer, DataFile> {

    /**
     * The current version of the data file.
     */
    public static final int VERSION = 1;

    @Override
    public void accept(final Integer integer, final DataFile dataFile) {
        // Do nothing.
    }
}
