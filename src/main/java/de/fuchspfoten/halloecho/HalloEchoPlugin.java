package de.fuchspfoten.halloecho;

import de.fuchspfoten.halloecho.modules.AdvancementBlockerModule;
import de.fuchspfoten.halloecho.modules.BlockUnicodeModule;
import de.fuchspfoten.halloecho.modules.ChatFormatterModule;
import de.fuchspfoten.halloecho.modules.ColorModule;
import de.fuchspfoten.halloecho.modules.DeathMessageModule;
import de.fuchspfoten.halloecho.modules.EmojiModule;
import de.fuchspfoten.halloecho.modules.JoinLeaveMessageModule;
import de.fuchspfoten.halloecho.modules.MentionModule;
import de.fuchspfoten.halloecho.modules.MessageModule;
import de.fuchspfoten.halloecho.modules.MuteChatModule;
import lombok.Getter;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * The main plugin class.
 */
public class HalloEchoPlugin extends JavaPlugin {

    /**
     * The singleton plugin instance.
     */
    private @Getter static HalloEchoPlugin self;

    /**
     * The chat formatter module.
     */
    private @Getter ChatFormatterModule chatFormatterModule;

    @Override
    public void onEnable() {
        self = this;

        // Create the default configuration.
        getConfig().options().copyDefaults(true);
        saveConfig();

        // Register command executor modules.
        // ...

        // Register modules.
        chatFormatterModule = new ChatFormatterModule();
        getServer().getPluginManager().registerEvents(new AdvancementBlockerModule(), this);
        getServer().getPluginManager().registerEvents(new BlockUnicodeModule(), this);
        getServer().getPluginManager().registerEvents(chatFormatterModule, this);
        getServer().getPluginManager().registerEvents(new ColorModule(), this);
        getServer().getPluginManager().registerEvents(new DeathMessageModule(), this);
        getServer().getPluginManager().registerEvents(new EmojiModule(getConfig()), this);
        getServer().getPluginManager().registerEvents(new JoinLeaveMessageModule(), this);
        getServer().getPluginManager().registerEvents(new MentionModule(), this);
        getServer().getPluginManager().registerEvents(new MuteChatModule(), this);

        // Register other modules.
        //noinspection unused Does initialization in constructor.
        final MessageModule msgModule = new MessageModule();
    }
}
