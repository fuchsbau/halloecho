package de.fuchspfoten.halloecho.modules;

import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerJoinEvent;

import java.util.Collection;
import java.util.HashSet;
import java.util.UUID;

/**
 * Allows colors for users with a certain permission.
 */
public class ColorModule implements Listener {

    /**
     * A set of UUIDs that may use color.
     */
    private final Collection<UUID> colorAllowed = new HashSet<>();

    @EventHandler(priority = EventPriority.LOW)
    public void onAsyncPlayerChat(final AsyncPlayerChatEvent event) {
        synchronized (colorAllowed) {
            if (!colorAllowed.contains(event.getPlayer().getUniqueId())) {
                return;
            }
        }

        // Translate color codes.
        event.setMessage(ChatColor.translateAlternateColorCodes('&', event.getMessage()));
    }

    @EventHandler
    public void onPlayerJoin(final PlayerJoinEvent event) {
        if (event.getPlayer().hasPermission("halloecho.color")) {
            colorAllowed.add(event.getPlayer().getUniqueId());
        }
    }
}
