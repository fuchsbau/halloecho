package de.fuchspfoten.halloecho.modules;

import de.fuchspfoten.fuchslib.ArgumentParser;
import de.fuchspfoten.fuchslib.Messenger;
import de.fuchspfoten.fuchslib.data.PlayerData;
import de.fuchspfoten.halloecho.HalloEchoPlugin;
import net.milkbowl.vault.chat.Chat;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.server.ServiceRegisterEvent;
import org.bukkit.event.server.ServiceUnregisterEvent;

import java.util.Collection;
import java.util.HashSet;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Formats the chat.
 */
public class ChatFormatterModule implements CommandExecutor, Listener {

    /**
     * The pattern for the prefix of player names.
     */
    private static final Pattern PREFIX_PATTERN = Pattern.compile("\\{prefix}");

    /**
     * The pattern for the suffix of player names.
     */
    private static final Pattern SUFFIX_PATTERN = Pattern.compile("\\{suffix}");

    /**
     * The pattern for the name of player name messages.
     */
    private static final Pattern NAME_PATTERN = Pattern.compile("\\{name}");

    /**
     * Set of UUIDs that override chat toggles.
     */
    private final Collection<UUID> chatoffOverride = new HashSet<>();

    /**
     * Set of UUIDs that have the chat disabled
     */
    private final Collection<UUID> chatOff = new HashSet<>();

    /**
     * The chat provider.
     */
    private Chat chat;

    /**
     * Argument parser for /togglechat.
     */
    private final ArgumentParser argumentParserToggleChat;

    /**
     * Constructor.
     */
    public ChatFormatterModule() {
        Messenger.register("halloecho.chatToggledError");
        Messenger.register("halloecho.toggledChatOn");
        Messenger.register("halloecho.toggledChatOff");

        reloadChat();
        argumentParserToggleChat = new ArgumentParser();

        Bukkit.getPluginCommand("togglechat").setExecutor(this);
    }

    /**
     * Reloads the chat.
     */
    private void reloadChat() {
        final Chat newChat = Bukkit.getServicesManager().load(Chat.class);
        if (newChat != chat) {
            chat = newChat;
        }
        if (chat == null) {
            HalloEchoPlugin.getSelf().getLogger().warning("No chat adapter after reload!");
        }
    }

    /**
     * Checks whether or not one UUID can hear another.
     *
     * @param sender   The sending UUID.
     * @param receiver The receiving UUID.
     * @return {@code true} iff the receiving UUID can hear the sending UUID.
     */
    public boolean canHear(final UUID sender, final UUID receiver) {
        synchronized (chatOff) {
            synchronized (chatoffOverride) {
                return !chatOff.contains(receiver)
                        || (chatoffOverride.contains(sender) && !chatoffOverride.contains(receiver));
            }
        }
    }

    @EventHandler
    public void onPlayerJoin(final PlayerJoinEvent event) {
        // Check for toggle-off.
        final PlayerData pd = new PlayerData(event.getPlayer());
        if (pd.hasFlag("halloecho.chatOff")) {
            synchronized (chatOff) {
                chatOff.add(event.getPlayer().getUniqueId());
            }
        }

        // Check for overrides.
        if (event.getPlayer().hasPermission("halloecho.chatOff.override")) {
            synchronized (chatoffOverride) {
                chatoffOverride.add(event.getPlayer().getUniqueId());
            }
        }
    }

    @EventHandler
    public void onServiceRegister(final ServiceRegisterEvent event) {
        if (event.getProvider().getService() == Chat.class) {
            reloadChat();
        }
    }

    @EventHandler
    public void onServiceUnregister(final ServiceUnregisterEvent event) {
        if (event.getProvider().getService() == Chat.class) {
            reloadChat();
        }
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onAsyncPlayerChatLowest(final AsyncPlayerChatEvent event) {
        event.setFormat("{prefix}{name}: {suffix}%2$s");
    }

    @EventHandler(priority = EventPriority.HIGH)
    public void onAsyncPlayerChatHigh(final AsyncPlayerChatEvent event) {
        if (event.isCancelled()) {
            return;
        }

        synchronized (chatOff) {
            // Block messages sent by chat togglers.
            if (chatOff.contains(event.getPlayer().getUniqueId())) {
                event.setCancelled(true);
                Messenger.send(event.getPlayer(), "halloecho.chatToggledError");
                return;
            }
        }

        // Remove recipients.
        event.getRecipients().removeIf(recipient -> !canHear(event.getPlayer().getUniqueId(), recipient.getUniqueId()));
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onAsyncPlayerChatHighest(final AsyncPlayerChatEvent event) {
        String fmt = event.getFormat();
        if (chat != null) {
            final String prefix = chat.getPlayerPrefix(event.getPlayer());
            fmt = PREFIX_PATTERN.matcher(fmt).replaceAll(
                    Matcher.quoteReplacement(ChatColor.translateAlternateColorCodes('&', prefix)));
            final String suffix = chat.getPlayerSuffix(event.getPlayer());
            fmt = SUFFIX_PATTERN.matcher(fmt).replaceAll(
                    Matcher.quoteReplacement(ChatColor.translateAlternateColorCodes('&', suffix)));
        }
        fmt = NAME_PATTERN.matcher(fmt).replaceAll(Matcher.quoteReplacement(event.getPlayer().getName()));
        event.setFormat(fmt);
    }

    @Override
    public boolean onCommand(final CommandSender sender, final Command command, final String label,
                             final String[] args) {
        // Player check.
        if (!(sender instanceof Player)) {
            sender.sendMessage("player-only!");
            return true;
        }

        // Syntax check.
        /*args = */
        argumentParserToggleChat.parse(args);
        if (args.length != 0 || argumentParserToggleChat.hasArgument('h')) {
            Messenger.send(sender, "commandHelp", "Syntax: /togglechat");
            argumentParserToggleChat.showHelp(sender);
            return true;
        }

        // Toggle the setting.
        synchronized (chatOff) {
            final PlayerData pd = new PlayerData((OfflinePlayer) sender);
            pd.toggleFlag("halloecho.chatOff");
            if (pd.hasFlag("halloecho.chatOff")) {
                Messenger.send(sender, "halloecho.toggledChatOff");
                chatOff.add(((Entity) sender).getUniqueId());
            } else {
                Messenger.send(sender, "halloecho.toggledChatOn");
                chatOff.remove(((Entity) sender).getUniqueId());
            }
        }

        return true;
    }
}
