package de.fuchspfoten.halloecho.modules;

import org.bukkit.ChatColor;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerJoinEvent;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;

/**
 * Inserts emojis.
 */
public class EmojiModule implements Listener {

    /**
     * The map of emoji replacements.
     */
    private final Map<String, String> replacements = new HashMap<>();

    /**
     * The set of UUIDs that may use emojis.
     */
    private final Collection<UUID> emojisAllowed = new HashSet<>();

    /**
     * Constructor.
     *
     * @param config The configuration.
     */
    public EmojiModule(final ConfigurationSection config) {
        for (final String origin : config.getConfigurationSection("emoji").getKeys(false)) {
            replacements.put(origin, config.getConfigurationSection("emoji").getString(origin));
        }
    }

    @EventHandler
    public void onPlayerJoin(final PlayerJoinEvent event) {
        if (event.getPlayer().hasPermission("halloecho.emoji")) {
            synchronized (emojisAllowed) {
                emojisAllowed.add(event.getPlayer().getUniqueId());
            }
        }
    }

    @EventHandler(priority = EventPriority.NORMAL)
    public void onAsyncPlayerChat(final AsyncPlayerChatEvent event) {
        synchronized (emojisAllowed) {
            if (!emojisAllowed.contains(event.getPlayer().getUniqueId())) {
                return;
            }
        }

        // Determine the words in the message.
        final Collection<String> words =
                new HashSet<>(Arrays.asList(ChatColor.stripColor(event.getMessage()).split(" ")));
        String message = event.getMessage();

        // Replace all emojis, if the permission was granted.
        synchronized (replacements) {
            for (final Entry<String, String> entry : replacements.entrySet()) {
                if (words.contains(entry.getKey())) {
                    while (message.contains(entry.getKey())) {
                        message = message.replace(entry.getKey(), "§f" + entry.getValue());
                    }
                }
            }
        }
        event.setMessage(message);
    }
}
