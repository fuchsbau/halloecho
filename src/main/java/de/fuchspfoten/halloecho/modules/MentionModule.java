package de.fuchspfoten.halloecho.modules;

import de.fuchspfoten.fuchslib.ArgumentParser;
import de.fuchspfoten.fuchslib.Messenger;
import de.fuchspfoten.fuchslib.data.PlayerData;
import de.fuchspfoten.halloecho.HalloEchoPlugin;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerJoinEvent;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.UUID;
import java.util.regex.Pattern;

/**
 * Parses mentions in chat messages.
 */
public class MentionModule implements CommandExecutor, Listener {

    /**
     * Pattern to split words for mentions.
     */
    private static final Pattern WORD_SPLIT = Pattern.compile("\\b");

    /**
     * Argument parser for /togglementions.
     */
    private final ArgumentParser argumentParserToggleMentions;

    /**
     * Set of UUIDs that override toggles.
     */
    private final Collection<UUID> overrideToggle = new HashSet<>();

    /**
     * Set of UUIDs that toggled mentions off.
     */
    private final Collection<UUID> toggledOff = new HashSet<>();

    /**
     * Contains mentionable strings with the corresponding UUIDs.
     */
    private final Map<String, UUID> mentionable = new HashMap<>();

    /**
     * Maps IDs to sets of visible IDs.
     */
    private final Map<UUID, Set<UUID>> visible = new HashMap<>();

    /**
     * The lock for async access.
     */
    private final Object lock = new Object();

    /**
     * Constructor.
     */
    public MentionModule() {
        Messenger.register("halloecho.mentionsOn");
        Messenger.register("halloecho.mentionsOff");
        Messenger.register("halloecho.targetToggledError");

        argumentParserToggleMentions = new ArgumentParser();

        Bukkit.getPluginCommand("togglementions").setExecutor(this);

        Bukkit.getScheduler().scheduleSyncRepeatingTask(HalloEchoPlugin.getSelf(), () -> {
            synchronized (lock) {
                mentionable.clear();
                Bukkit.getOnlinePlayers().forEach(x -> mentionable.put(x.getName(), x.getUniqueId()));

                Bukkit.getOnlinePlayers().forEach(x -> {
                    final Set<UUID> visibleIds = visible.computeIfAbsent(x.getUniqueId(), u -> new HashSet<>());
                    visibleIds.clear();
                    //noinspection Convert2MethodRef compiler doesn't support that?
                    Bukkit.getOnlinePlayers().stream()
                            .filter(y -> x.canSee(y))
                            .map(Entity::getUniqueId)
                            .forEach(visibleIds::add);
                });
            }
        }, 20L, 20L);
    }

    @EventHandler
    public void onPlayerJoin(final PlayerJoinEvent event) {
        // Check for toggle-off.
        final PlayerData pd = new PlayerData(event.getPlayer());
        if (pd.hasFlag("halloecho.mentionsOff")) {
            toggledOff.add(event.getPlayer().getUniqueId());
        }

        // Check for overrides.
        if (event.getPlayer().hasPermission("halloecho.mention.override")) {
            synchronized (lock) {
                overrideToggle.add(event.getPlayer().getUniqueId());
            }
        }
    }

    @EventHandler(priority = EventPriority.NORMAL)
    public void onAsyncPlayerChat(final AsyncPlayerChatEvent event) {
        // Determine whether the sender overrides mention toggles.
        final boolean senderOverrides;
        synchronized (lock) {
            senderOverrides = overrideToggle.contains(event.getPlayer().getUniqueId());
        }

        // Determine the words in the message.
        final Collection<String> words =
                new HashSet<>(Arrays.asList(WORD_SPLIT.split(ChatColor.stripColor(event.getMessage()))));
        String message = event.getMessage();

        final Collection<UUID> toPing = new LinkedList<>();
        synchronized (lock) {
            for (final Entry<String, UUID> entry : mentionable.entrySet()) {
                if ((entry.getKey()).equals(event.getPlayer().getName())) {
                    continue;
                }

                // Check target visibility.
                final Set<UUID> visibleSet = visible.get(event.getPlayer().getUniqueId());
                if (visibleSet != null) {
                    if (!visibleSet.contains(entry.getValue())) {
                        continue;
                    }
                } else {
                    continue;
                }

                if (words.contains(entry.getKey())) {
                    message = message.replaceAll(entry.getKey(), "§8" + entry.getKey() + "§f");
                    toPing.add(entry.getValue());
                }
            }
        }

        // Update the message and ping mentioned players.
        event.setMessage(message);
        Bukkit.getScheduler().scheduleSyncDelayedTask(HalloEchoPlugin.getSelf(),
                () -> toPing.forEach(id -> {
                    final ChatFormatterModule cfm = HalloEchoPlugin.getSelf().getChatFormatterModule();
                    if (cfm.canHear(event.getPlayer().getUniqueId(), id)) {
                        mentionNotify(id, senderOverrides);
                    } else {
                        Messenger.send(event.getPlayer(), "halloecho.targetToggledError");
                    }
                }));
    }

    @Override
    public boolean onCommand(final CommandSender sender, final Command command, final String label,
                             final String[] args) {
        // Player check.
        if (!(sender instanceof Player)) {
            sender.sendMessage("player-only!");
            return true;
        }

        // Syntax check.
        /*args = */argumentParserToggleMentions.parse(args);
        if (args.length != 0 || argumentParserToggleMentions.hasArgument('h')) {
            Messenger.send(sender, "commandHelp", "Syntax: /togglementions");
            argumentParserToggleMentions.showHelp(sender);
            return true;
        }

        // Toggle the setting.
        final PlayerData pd = new PlayerData((OfflinePlayer) sender);
        pd.toggleFlag("halloecho.mentionsOff");
        if (pd.hasFlag("halloecho.mentionsOff")) {
            Messenger.send(sender, "halloecho.mentionsOff");
            toggledOff.add(((Entity) sender).getUniqueId());
        } else {
            Messenger.send(sender, "halloecho.mentionsOn");
            toggledOff.remove(((Entity) sender).getUniqueId());
        }

        return true;
    }

    /**
     * Mentions the given UUID, if allowed.
     *
     * @param id The UUID.
     * @param override Whether to skip permission checks.
     */
    private void mentionNotify(final UUID id, final boolean override) {
        // Check whether the action is permitted.
        if (!override) {
            if (toggledOff.contains(id)) {
                return;
            }
        }

        // Fetch the target.
        final Player target = Bukkit.getPlayer(id);
        if (target == null || !target.isOnline()) {
            return;
        }

        // Notify the target.
        target.playSound(target.getLocation(), Sound.ENTITY_ENDERMEN_TELEPORT, 1.0f, 1.0f);
    }
}
