package de.fuchspfoten.halloecho.modules;

import de.fuchspfoten.fuchslib.ArgumentParser;
import de.fuchspfoten.fuchslib.Messenger;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

/**
 * /mutechat disables the complete chat.
 */
public class MuteChatModule implements CommandExecutor, Listener {

    /**
     * Argument parser for /mutechat.
     */
    private final ArgumentParser argumentParserMuteChat;

    /**
     * Whether or not the chat is muted.
     */
    private boolean chatMuted;

    /**
     * The lock for accessing {@link #chatMuted}.
     */
    private final Object lock = new Object();

    /**
     * Constructor.
     */
    public MuteChatModule() {
        Messenger.register("halloecho.chatOn");
        Messenger.register("halloecho.chatOff");
        Messenger.register("halloecho.chatOffError");

        argumentParserMuteChat = new ArgumentParser();

        Bukkit.getPluginCommand("mutechat").setExecutor(this);
    }

    @EventHandler(priority = EventPriority.HIGH)
    public void onAsyncPlayerChat(final AsyncPlayerChatEvent event) {
        if (event.isCancelled()) {
            return;  // Might be low-level conversation handling...
        }

        // Check muted state.
        final boolean muted;
        synchronized (lock) {
            muted = chatMuted;
        }

        // Block messages if muted.
        if (muted) {
            event.setCancelled(true);
            Messenger.send(event.getPlayer(), "halloecho.chatOffError");
        }
    }

    @Override
    public boolean onCommand(final CommandSender sender, final Command command, final String label,
                             final String[] args) {
        // Permission check.
        if (!sender.hasPermission("halloecho.mutechat")) {
            sender.sendMessage("Missing permission: halloecho.mutechat");
            return true;
        }

        // Syntax check.
        /*args = */
        argumentParserMuteChat.parse(args);
        if (args.length != 0 || argumentParserMuteChat.hasArgument('h')) {
            Messenger.send(sender, "commandHelp", "Syntax: /mutechat");
            argumentParserMuteChat.showHelp(sender);
            return true;
        }

        // Toggle the setting.
        final String msgFormat;
        synchronized (lock) {
            chatMuted = !chatMuted;
            if (chatMuted) {
                msgFormat = "halloecho.chatOff";
            } else {
                msgFormat = "halloecho.chatOn";
            }
        }

        // Notify the players.
        Bukkit.getOnlinePlayers().forEach(p -> Messenger.send(p, msgFormat, sender.getName()));

        return true;
    }
}
