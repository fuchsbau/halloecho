package de.fuchspfoten.halloecho.modules;

import de.fuchspfoten.fuchslib.Messenger;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * /msg &lt;player&gt; &lt;msg&gt; whispers to other players.
 * /r &lt;msg&gt; replies to messages.
 */
public class MessageModule implements CommandExecutor {

    /**
     * Maps UUIDs to the UUIDs replies are directed to.
     */
    private final Map<UUID, UUID> replyToMap = new HashMap<>();

    /**
     * Constructor.
     */
    public MessageModule() {
        Messenger.register("halloecho.msg.inbound");
        Messenger.register("halloecho.msg.outbound");
        Messenger.register("halloecho.msg.playerNotFound");
        Messenger.register("halloecho.msg.replyNotFound");

        Bukkit.getPluginCommand("msg").setExecutor(this);
        Bukkit.getPluginCommand("r").setExecutor(this);
    }

    @Override
    public boolean onCommand(final CommandSender sender, final Command command, final String label, String[] args) {
        if (!(sender instanceof Player)) {
            sender.sendMessage("player-only");
            return true;
        }
        final Player player = (Player) sender;

        // Permission check.
        if (!sender.hasPermission("halloecho.msg")) {
            sender.sendMessage("Missing permission: halloecho.msg");
            return true;
        }

        Player target = null;
        if (command.getName().equals("r")) {
            // Fetch target with /r.
            final UUID replyTarget = replyToMap.get(((Entity) sender).getUniqueId());
            target = (replyTarget != null) ? Bukkit.getPlayer(replyTarget) : null;
            if (target == null || !target.isOnline() || !player.canSee(target)) {
                Messenger.send(sender, "halloecho.msg.replyNotFound");
                return true;
            }
        } else if (args.length >= 1) {
            // Fetch target with /msg.
            target = Bukkit.getPlayer(args[0]);
            args = Arrays.copyOfRange(args, 1, args.length);
            if (target == null || !target.isOnline() || !player.canSee(target)) {
                Messenger.send(sender, "halloecho.msg.playerNotFound");
                return true;
            }
        }

        // Check that there is a target and a message.
        if (target == null || args.length < 1) {
            Messenger.send(sender, "commandHelp", "Syntax: /msg <name> <msg>");
            Messenger.send(sender, "commandHelp", "Syntax: /r <msg>");
            return true;
        }

        // Construct the message.
        final StringBuilder messageBuilder = new StringBuilder();
        for (final String arg : args) {
            messageBuilder.append(arg).append(' ');
        }
        final String message = messageBuilder.toString();

        // Deliver the messages.
        Messenger.send(sender, "halloecho.msg.outbound", sender.getName(), target.getName(), message);
        Messenger.send(target, "halloecho.msg.inbound", sender.getName(), target.getName(), message);

        // Update the reply map.
        replyToMap.put(player.getUniqueId(), target.getUniqueId());
        replyToMap.put(target.getUniqueId(), player.getUniqueId());

        return true;
    }
}
