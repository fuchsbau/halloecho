package de.fuchspfoten.halloecho.modules;

import de.fuchspfoten.fuchslib.Messenger;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

/**
 * Changes join and leave messages.
 */
public class JoinLeaveMessageModule implements Listener {

    /**
     * Constructor.
     */
    public JoinLeaveMessageModule() {
        Messenger.register("halloecho.join");
        Messenger.register("halloecho.leave");
    }

    @EventHandler
    public void onPlayerJoin(final PlayerJoinEvent event) {
        event.setJoinMessage(String.format(Messenger.getFormat("halloecho.join"), event.getPlayer().getName()));
    }

    @EventHandler
    public void onPlayerQuit(final PlayerQuitEvent event) {
        event.setQuitMessage(String.format(Messenger.getFormat("halloecho.leave"), event.getPlayer().getName()));
    }
}
