package de.fuchspfoten.halloecho.modules;

import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.world.WorldLoadEvent;

/**
 * Disables advancement messages.
 */
public class AdvancementBlockerModule implements Listener {

    /**
     * Constructor.
     */
    public AdvancementBlockerModule() {
        for (final World world : Bukkit.getWorlds()) {
            world.setGameRuleValue("announceAdvancements", "false");
        }
    }

    @EventHandler
    public void onWorldLoad(final WorldLoadEvent event) {
        event.getWorld().setGameRuleValue("announceAdvancements", "false");
    }
}
