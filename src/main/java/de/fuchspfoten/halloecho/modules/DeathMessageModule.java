package de.fuchspfoten.halloecho.modules;

import de.fuchspfoten.fuchslib.Messenger;
import de.fuchspfoten.fuchslib.data.DataFile;
import de.fuchspfoten.halloecho.DeathMessageVersionUpdater;
import de.fuchspfoten.halloecho.HalloEchoPlugin;
import org.bukkit.Material;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByBlockEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.entity.PlayerDeathEvent;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Random;

/**
 * Changes death messages.
 */
public class DeathMessageModule implements Listener {

    /**
     * The random number generator used for death message selection.
     */
    private final Random random = new Random();

    /**
     * The data file from which death messages are sourced.
     */
    private final DataFile deathMessageData;

    /**
     * Constructor.
     */
    public DeathMessageModule() {
        Messenger.register("halloecho.defaultDeathMessage");

        deathMessageData = new DataFile(new File(HalloEchoPlugin.getSelf().getDataFolder(), "death.yml"),
                new DeathMessageVersionUpdater());
    }

    @EventHandler
    public void onPlayerDeath(final PlayerDeathEvent event) {
        final EntityDamageEvent lastDamage = event.getEntity().getLastDamageCause();
        if (lastDamage == null) {
            return;
        }

        if (lastDamage instanceof EntityDamageByBlockEvent) {
            event.setDeathMessage(handleBlockCause((EntityDamageByBlockEvent) lastDamage));
            return;
        }

        if (lastDamage instanceof EntityDamageByEntityEvent) {
            event.setDeathMessage(handleEntityCause((EntityDamageByEntityEvent) lastDamage));
            return;
        }

        event.setDeathMessage(handleGenericCause(lastDamage));
    }

    /**
     * Handles a generic death cause.
     *
     * @param event The death-causing damage event.
     * @return The death message.
     */
    private String handleGenericCause(final EntityDamageEvent event) {
        if (event.getCause() == DamageCause.FIRE || event.getCause() == DamageCause.FIRE_TICK) {
            return getDeathMessage("generic.fire", event.getEntity().getName());
        }
        if (event.getCause() == DamageCause.DROWNING) {
            return getDeathMessage("generic.drowning", event.getEntity().getName());
        }
        if (event.getCause() == DamageCause.SUFFOCATION) {
            return getDeathMessage("generic.suffocation", event.getEntity().getName());
        }
        if (event.getCause() == DamageCause.FALL) {
            return getDeathMessage("generic.fall", event.getEntity().getName());
        }

        HalloEchoPlugin.getSelf().getLogger().info(String.format("unknown death by cause %1$s",
                event.getCause().name()));
        return String.format(Messenger.getFormat("halloecho.defaultDeathMessage"), event.getEntity().getName());
    }

    /**
     * Handles a block as a death cause.
     *
     * @param event The death-causing damage event.
     * @return The death message.
     */
    private String handleBlockCause(final EntityDamageByBlockEvent event) {
        if (event.getCause() == DamageCause.CONTACT) {
            if (event.getDamager() != null) {
                if (event.getDamager().getType() == Material.CACTUS) {
                    return getDeathMessage("block.contact.cactus", event.getEntity().getName());
                }
            }
        } else if (event.getCause() == DamageCause.HOT_FLOOR) {
            if (event.getDamager() != null) {
                if (event.getDamager().getType() == Material.MAGMA) {
                    return getDeathMessage("block.hot_floor.magma", event.getEntity().getName());
                }
            }
        } else if (event.getCause() == DamageCause.LAVA) {
            return getDeathMessage("block.lava", event.getEntity().getName());
        } else if (event.getCause() == DamageCause.BLOCK_EXPLOSION) {
            return getDeathMessage("block.explosion", event.getEntity().getName());
        }

        HalloEchoPlugin.getSelf().getLogger().info(String.format("unknown death by cause %1$s with block %2$s",
                event.getCause().name(), event.getDamager() != null ? event.getDamager().getType() : "null"));
        return String.format(Messenger.getFormat("halloecho.defaultDeathMessage"), event.getEntity().getName());
    }

    /**
     * Handles an entity as a death cause.
     *
     * @param event The death-causing damage event.
     * @return The death message.
     */
    private String handleEntityCause(final EntityDamageByEntityEvent event) {
        if (event.getCause() == DamageCause.ENTITY_EXPLOSION) {
            if (event.getDamager() != null) {
                final String type = event.getDamager().getType().name().toLowerCase();
                return getDeathMessage("entity.explosion." + type, event.getEntity().getName());
            }
        } else if (event.getCause() == DamageCause.ENTITY_ATTACK) {
            if (event.getDamager() != null) {
                final String type = event.getDamager().getType().name().toLowerCase();
                return getDeathMessage("entity.attack." + type, event.getEntity().getName());
            }
        } else if (event.getCause() == DamageCause.PROJECTILE) {
            if (event.getDamager() != null && event.getDamager() instanceof Projectile
                    && ((Projectile) event.getDamager()).getShooter() instanceof Entity) {
                final String type = event.getDamager().getType().name().toLowerCase();
                final String shooterType =
                        ((Entity) ((Projectile) event.getDamager()).getShooter()).getType().name().toLowerCase();
                return getDeathMessage("entity.projectile." + type + '.' + shooterType,
                        event.getEntity().getName());
            }
        } else if (event.getCause() == DamageCause.MAGIC) {
            if (event.getDamager() != null && event.getDamager() instanceof Projectile
                    && ((Projectile) event.getDamager()).getShooter() instanceof Entity) {
                final String type = event.getDamager().getType().name().toLowerCase();
                final String shooterType =
                        ((Entity) ((Projectile) event.getDamager()).getShooter()).getType().name().toLowerCase();
                return getDeathMessage("entity.magic." + type + '.' + shooterType, event.getEntity().getName());
            }
        }

        HalloEchoPlugin.getSelf().getLogger().info(String.format("unknown death by cause %1$s with entity %2$s",
                event.getCause().name(), event.getDamager() != null ? event.getDamager().getType() : "null"));
        return String.format(Messenger.getFormat("halloecho.defaultDeathMessage"), event.getEntity().getName());
    }

    /**
     * Obtains a death message from the pool with the given key.
     *
     * @param key  The pool key.
     * @param name The name of the victim.
     * @return The death message from the pool.
     */
    private String getDeathMessage(final String key, final String name) {
        return getDeathMessage(key, name, null);
    }

    /**
     * Obtains a death message from the pool with the given key.
     *
     * @param key  The pool key.
     * @param name The name of the victim.
     * @param data Additional data regarding the death.
     * @return The death message from the pool.
     */
    private String getDeathMessage(final String key, final String name, final String data) {
        if (!deathMessageData.getStorage().isList(key)) {
            // Reload before saving to keep changes not reflected in memory.
            deathMessageData.reload();

            // Set the default value for the list.
            final Collection<String> list = new ArrayList<>();
            list.add(Messenger.getFormat("halloecho.defaultDeathMessage"));
            deathMessageData.getStorage().set(key, list);

            // Save the data.
            deathMessageData.save(DeathMessageVersionUpdater.VERSION);
        }

        final List<String> pool = deathMessageData.getStorage().getStringList(key);
        return String.format(pool.get(random.nextInt(pool.size())), name, data);
    }
}
