package de.fuchspfoten.halloecho.modules;

import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;

import java.util.regex.Pattern;

/**
 * Blocks unicode characters.
 */
public class BlockUnicodeModule implements Listener {

    /**
     * The range of banned characters.
     */
    private static final Pattern BANNED_RANGE = Pattern.compile("[^\\u0020-\\u007E\\u00A1-\\u00FF]");

    @EventHandler(priority = EventPriority.LOWEST)
    public void onAsyncPlayerChat(final AsyncPlayerChatEvent event) {
        event.setMessage(BANNED_RANGE.matcher(event.getMessage()).replaceAll("?"));
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onPlayerCommandPreprocess(final PlayerCommandPreprocessEvent event) {
        if (!event.getPlayer().hasPermission("halloecho.unicode")) {
            event.setMessage(BANNED_RANGE.matcher(event.getMessage()).replaceAll("?"));
        }
    }
}
